#include <stdbool.h>

#define HEAP_BIG_SIZE 8192
#define HEAP_SMALL_SIZE 10
#define BLOCK_PADDING 128

bool normal_success_memory_allocate();
bool freeing_one_block_from_several_allocated();
bool freeing_two_blocks_from_multiple_allocations();
bool memory_over_new_region_expands_old();
bool memory_over_old_cant_expanded_new_allocated_elsewhere();