#include "tests.h"
#include <stdio.h>

int main() {
    if (normal_success_memory_allocate() && freeing_one_block_from_several_allocated() &&
    freeing_two_blocks_from_multiple_allocations() && memory_over_new_region_expands_old() &&
    memory_over_old_cant_expanded_new_allocated_elsewhere()) {
        printf("All test passed");
    } else {
        printf("Found error, read previous messages");
    }
    return 0;
}
