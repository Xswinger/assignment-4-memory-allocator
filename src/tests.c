#define _DEFAULT_SOURCE

#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"
#include "tests.h"

bool normal_success_memory_allocate() {
    printf("First test running...");

    void* heap = heap_init(HEAP_BIG_SIZE);
    void *block = _malloc(BLOCK_PADDING);

    if (!heap || !block) {
        printf("Error when run first test, test stop");
        return false;
    }

    debug_heap(stdout, heap);
    _free(block);

    munmap(heap, size_from_capacity((block_capacity) {.bytes=HEAP_BIG_SIZE}).bytes);
    return true;
}

bool freeing_one_block_from_several_allocated() {
    printf("Second test running...");

    void* heap = heap_init(HEAP_BIG_SIZE);
    void *block = _malloc(BLOCK_PADDING);
    void *freeing_block = _malloc(BLOCK_PADDING);

    debug_heap(stdout, heap);

    if (!heap || !block || !freeing_block) {
        printf("Error when run second test, test stop");
        return false;
    }

    _free(freeing_block);
    debug_heap(stdout, heap);
    _free(block);
    munmap(heap, size_from_capacity((block_capacity) {.bytes=HEAP_BIG_SIZE}).bytes);
    return true;
}

bool freeing_two_blocks_from_multiple_allocations() {
    printf("Third test running...");

    void* heap = heap_init(HEAP_BIG_SIZE);
    void *block_1 = _malloc(BLOCK_PADDING);
    void *freeing_block_1 = _malloc(BLOCK_PADDING);
    void *block_2 = _malloc(BLOCK_PADDING);
    void *block_3 = _malloc(BLOCK_PADDING);
    void *freeing_block_2 = _malloc(BLOCK_PADDING);
    debug_heap(stdout, heap);

    if (!heap || !block_1 || !block_2 || !block_3 || !freeing_block_1 || !freeing_block_2) {
        printf("Error when run third test, test stop");
        return false;
    }

    _free(freeing_block_1);
    _free(freeing_block_2);
    debug_heap(stdout, heap);
    _free(block_1);
    _free(block_2);
    _free(block_3);

    munmap(heap, size_from_capacity((block_capacity) {.bytes=HEAP_BIG_SIZE}).bytes);
    return true;
}

bool memory_over_new_region_expands_old() {
    printf("Fourth test running...");

    void* heap = heap_init(HEAP_SMALL_SIZE);
    void *block = _malloc(BLOCK_PADDING);

    debug_heap(stdout, heap);

    if (!heap || !block) {
        printf("Error when run fourth test, test stop");
        return false;
    }

    _free(block);

    munmap(heap, size_from_capacity((block_capacity) {.bytes=HEAP_SMALL_SIZE + 128}).bytes);
    return true;
}

bool memory_over_old_cant_expanded_new_allocated_elsewhere() {
    printf("Fifth test running...");

    void* heap = heap_init(HEAP_BIG_SIZE);

    debug_heap(stdout, heap);

    (void) mmap(heap + HEAP_BIG_SIZE, HEAP_BIG_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);

    void *block = _malloc(HEAP_BIG_SIZE);

    if (!heap || !block) {
        printf("Error when run fifth test, test stop");
        return false;
    }

    debug_heap(stdout, heap);
    _free(block);

    munmap(heap, size_from_capacity((block_capacity) {.bytes=HEAP_BIG_SIZE}).bytes);
    return true;
}